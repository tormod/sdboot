* assemble/link as raw ROM

	.globl parsecmd
	.globl nopars
	.globl common
	.globl _blk_op_lba
	.globl basrom
	.globl voutstr
	.globl voutchr
	.globl vget16
	.globl vcmdlin

	.area cartstart

magic	fcc	'DK'
	bra	startdos
	bra	execme
	bra	copyrom

	* TODO possible options:
	* check for D key -> skip SD card
	* check for S key -> skip DWLOAD ?
	* try DWLOAD

	* things from original EXTBAS
	* enable PIA0 for 50Hz interrupt and enable interrupts?
	* warm start address? or jump to A0E2-5 to set default one?
	* jump to BASIC A0E2

signon	fcc	"SDBOOT ROM V1"
	fcb	$0D,$00

* copy code down to $0E00 (cartridge ROM will unmapped later)
copyrom
	ldx	#tocopy
	ldu	#$0E00		; start of .common in .link file
copyus	ldd	,x++
	std	,u++
; copy until end of text segment (must be last)
	cmpx	#__base_.text__+__len_.text__-ramcode+tocopy
	blo	copyus
	rts

* entry point from BASIC EXEC, allows options
execme	jsr	copyrom
	jsr	basrom-ramcode+tocopy		; detect BASIC ROM
	jmp	parsecmd

* entry from BASIC startup DOS cartridge detection
startdos
	; basrom is linked with RAM address, but we call the "original" in ROM
	; because it isn't copied yet
	jsr	basrom-ramcode+tocopy		; detect BASIC ROM
	ldx	#signon-1
	jsr	[voutstr]

* check for SHIFT key

chkshift lda   $FF02		; save PIA
         ldb   #$7F
         stb   $FF02
         ldb   $FF00
         sta   $FF02		; restore PIA
         comb
         andb  #$40
         beq   goram
         jmp   [vcmdlin]	; return if shift key pressed

goram	jsr	copyrom			; this will trash our BASIC vectors
	jsr	basrom-ramcode+tocopy	; so rewrite them again
	jmp	runram

* the code below will follow in the ROM image
* but is linked to run in the RAM copy of it
tocopy	equ *

	.area	ramstart

ramcode
	; SDBDOS V1 had NOP here (at $E00) and no function table
	jmp	runram		; filler (entry only used for DECB)
	fdb	loaddecb	; function table
	fdb	direct

; fixed location variables .vars follows

	.area	.main
runram
* initialize flag to not run sector dump test
	lda	#1
	sta	dumpflag

* check for pressed BREAK key
	lda   $FF02		; save PIA
	ldb   #$FF-$04
	stb   $FF02
	ldb   $FF00
	sta   $FF02		; restore PIA
	comb
	andb  #$40
	beq   defsd
* try SD card from alternate start sector
	ldx   #192
	stx   _blk_op_lba+2
	jsr   common
	bra   gobasic

* try SD card from default sector
defsd	jsr	nopars		; FIXME return here might be broken

gobasic	jmp	[vcmdlin]
	; jmp	$A0E2

