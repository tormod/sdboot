* Replace Extended BASIC with SDBOOT loader
* assemble/link as raw ROM

OUTSTR	equ $B99C	; CoCo ROM
CMDLINE	equ $A0D5

	.globl nopars

	.area romstart

magic	fcc	'EX'
	bra	start

	* TODO possible options:
	* check for D key -> skip SD card
	* check for S key -> skip DWLOAD ?
	* try DWLOAD

	* things from original EXTBAS
	* enable PIA0 for 50Hz interrupt and enable interrupts?
	* warm start address? or jump to A0E2-5 to set default one?
	* jump to BASIC A0E2

signon	fcc	"SDBOOT EXT ROM"
	fcb	$0D,$00

start
	ldx	#signon-1
	jsr	OUTSTR

* check for SHIFT key

chkshift lda   $FF02		; save PIA
         ldb   #$7F
         stb   $FF02
         ldb   $FF00
         sta   $FF02		; restore PIA
         comb
         andb  #$40
         lbne  CMDLINE		; return if shift key pressed

* copy other code down to $0E00 (BASIC ROM will unmapped later)

	ldx	#tocopy
	ldu	#$0E00		; start of .common in .link file
copyus	ldd	,x++
	std	,u++
; copy until end of text segment (must be last)
	cmpx	#__base_.text__+__len_.text__-ramcode+tocopy
	blo	copyus

	jsr	basrom-ramcode+tocopy	; write BASIC vectors
	jmp	ramcode

tocopy	equ *

	.area	ramstart

ramcode
	jmp	runram
	fdb	loaddecb
	fdb	direct

; fixed location variables .vars follow

	.area	.main

runram
* initialize flag to not run sector dump test
	lda	#1
	sta	dumpflag

* try SD card from default sector
	jsr	nopars		; FIXME return here might be broken

	jmp	CMDLINE
	jmp	$A0E2

