
CROSS_AS = m6809-unknown-as
CROSS_LD = lwlink
CROSS_CC = m6809-unknown-gcc
CROSS_CCOPTS = -c -Wall -Os -msoft-reg-count=0 -mfar-stack-param -I.

all: sdboot

.SUFFIXES:
.SUFFIXES: .c .s

AOBJS = spi.o standalone.o
COBJS = devsd.o devsd_discard.o
OBJS  = $(COBJS) $(AOBJS)

$(COBJS): %.o : %.c
	$(CROSS_CC) $(CROSS_CCOPTS) $<

$(AOBJS): %.o : %.s
	$(CROSS_AS) $(CROSS_ASOPTS) -o $*.o $<

sdboot: $(OBJS) sdboot.link
	$(CROSS_LD) -o $@ --script=$@.link --map=$@.map $(OBJS)

sdboot.wav: sdboot
	makewav -r -c -nSDBOOT -osdboot.wav $<

clean:
	rm -f $(OBJS) sdboot sdboot.wav sdboot.map
